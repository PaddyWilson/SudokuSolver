#pragma once
#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <time.h>
#include <cstdlib>
#include <windows.h>
#include <string>

using namespace std;

class SuDoKu
{
public:
	SuDoKu();
	~SuDoKu();

	void DrawBoard();

	bool CheckCoords(int row, int col, int number);
	bool CheckRow(int row, int number);
	bool CheckCol(int col, int number);
	bool Check33Cell(int row, int col, int number);

	void ReadFile(string filePath);

	bool IsSolved();

	//should have getter/setter 
	int board[9][9];//the game board
	bool change[9][9];//the original numbers

	bool SolveBruteForce();//fastest so far
	bool SolveBruteForceV2();//slower than v1
	bool SolveBruteForceV3();

private:
	bool SolveBruteForce(int x, int y);
	bool SolveBruteForceV2(int row, int col);
	bool SolveBruteForceV3(int x, int y);
};