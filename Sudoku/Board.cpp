#include "stdafx.h"
#include "Board.h"
#include <vector>

SuDoKu::SuDoKu()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			board[i][j] = 0;
			change[i][j] = true;
		}
	}
}

SuDoKu::~SuDoKu() {}

void setcolor(unsigned short color)                 //The function that you'll use to
{                                                   //set the colour
	HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hcon, color);
}

void SuDoKu::DrawBoard()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (change[i][j] == false)
			{
				setcolor(12);
				cout << board[i][j] << " ";
				setcolor(7);
			}
			else cout << board[i][j] << " ";
			if ((j + 1) % 3 == 0) cout << "| ";
		}
		cout << endl;
		if ((i + 1) % 3 == 0) cout << "------+-------+---------" << endl;
	}
}

bool SuDoKu::CheckCoords(int row, int col, int number)
{
	if (board[row][col] == number)
		return true;
	return false;
}

bool SuDoKu::CheckRow(int row, int number)
{
	for (int i = 0; i < 9; i++)
		if (board[row][i] == number)
			return true;
	return false;
}

bool SuDoKu::CheckCol(int col, int number)
{
	for (int i = 0; i < 9; i++)
		if (board[i][col] == number)
			return true;

	return false;
}

bool SuDoKu::Check33Cell(int row, int col, int number)
{
	int i = 0;
	int j = 0;
	int iMax = 0;
	int jMax = 0;

	if (row >= 0 && row <= 2)
	{
		i = 0;
		iMax = 3;
	}
	else if (row >= 3 && row <= 5)
	{
		i = 3;
		iMax = 6;
	}
	else if (row >= 6 && row <= 8)
	{
		i = 6;
		iMax = 9;
	}

	if (col >= 0 && col <= 2)
	{
		j = 0;
		jMax = 3;
	}
	else if (col >= 3 && col <= 5)
	{
		j = 3;
		jMax = 6;
	}
	else if (col >= 6 && col <= 8)
	{
		j = 6;
		jMax = 9;
	}
	int tempj = j;
	for (i = i; i < iMax; i++)
	{
		for (j = j; j < jMax; j++)
		{
			if (board[i][j] == number)
				return true;
		}
		j = tempj;
	}
	return false;
}

void SuDoKu::ReadFile(string filePath)
{
	int place = 0;
	for (int row = 0; row < 9; row++)
	{
		for (int i = 0; i < 9; i++)
		{
			if (filePath[place] == '.')
			{
				board[row][i] = 0;
			}
			else
			{
				board[row][i] = filePath[place] - '0';
				change[row][i] = false;
			}
			place++;
		}
	}
}

bool SuDoKu::IsSolved()
{
	return false;
}


bool SuDoKu::SolveBruteForce()
{
	return SolveBruteForce(0, -1);
}

bool SuDoKu::SolveBruteForce(int x, int y)
{
	int i = 1;
	y++;
	if (y >= 9)
	{
		y = 0;
		x++;
	}

	while (true)
	{
		if (change[x][y] == false)
		{
			if (x == 8 && y == 8)//at the end of the board
				return true;

			return SolveBruteForce(x, y);

			/*if (!SolveBruteForce(x, y))
				return false;
			else
				return true;*/
		}

		if (!Check33Cell(x, y, i) && !CheckCol(y, i) && !CheckRow(x, i))//check a the 3x3 cell, row, and col for the number
		{
			board[x][y] = i;
			if (x == 8 && y == 8)//at the end of the board
				return true;

			if (SolveBruteForce(x, y))//move to next coord
				return true;
		}
		else
		{
			i++;
			if (i > 9)
			{
				board[x][y] = 0;
				return false;
			}
		}
	}
}
//****************************************
bool SuDoKu::SolveBruteForceV2()
{
	return SolveBruteForceV2(0, -1);
}

bool SuDoKu::SolveBruteForceV2(int x, int y)
{
	y++;
	if (y >= 9)
	{
		y = 0;
		x++;
	}

	if (change[x][y] == false)//check if the number can be changed
	{
		if (x == 8 && y == 8)//at the end of the board
			return true;

		return SolveBruteForceV2(x, y);
	}

	vector<int> possibleNumbers;

	for (int m = 1; m <= 9; m++)
	{
		if (!Check33Cell(x, y, m) && !CheckCol(y, m) && !CheckRow(x, m))//check a the 3x3 cell, row, and col for the number
		{
			possibleNumbers.push_back(m);
		}
	}

	if (possibleNumbers.empty())//no numbers possible
	{
		board[x][y] = 0;
		return false;
	}

	while (!possibleNumbers.empty())
	{
		board[x][y] = possibleNumbers.back();
		if (!SolveBruteForceV2(x, y))
		{
			possibleNumbers.pop_back();
		}
		else
			return true;
	}
	board[x][y] = 0;
	return false;
}
//****************************************
bool SuDoKu::SolveBruteForceV3()
{

	return SolveBruteForceV3(0, -1);
}

bool SuDoKu::SolveBruteForceV3(int x, int y)
{
	y++;
	if (y >= 9)
	{
		y = 0;
		x++;
	}

	if (change[x][y] == false)//check if the number can be changed
	{
		if (x == 8 && y == 8)//at the end of the board
			return true;

		return SolveBruteForceV3(x, y);
	}

	vector<int> possibleNumbers;

	for (int m = 1; m <= 9; m++)
	{
		if (!Check33Cell(x, y, m) && !CheckCol(y, m) && !CheckRow(x, m))//check a the 3x3 cell, row, and col for the number
		{
			possibleNumbers.push_back(m);
		}
	}

	if (possibleNumbers.empty())//no numbers possible
	{
		board[x][y] = 0;
		return false;
	}

	while (!possibleNumbers.empty())
	{
		board[x][y] = possibleNumbers.back();
		if (!SolveBruteForceV3(x, y))
		{
			possibleNumbers.pop_back();
		}
		else
			return true;
	}
	board[x][y] = 0;
	return false;
}
