// Sudoku.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <time.h>
#include <cstdlib>
#include <windows.h>
#include <vector>
#include <chrono>
#include <time.h>

#include <thread>

#include "Board.h"

#include <fstream>

using namespace std;

void ThreadStart(int number);
void ThreadPro();

vector<string> puzzels;

int main()
{
	//path to file
	std::ifstream file("D:\\Programming Things\\SudokuSolver\\Puzzles\\number1.txt");
	std::string str;
	int place = 0;
	while (std::getline(file, str))
	{
		puzzels.push_back(str);
	}

	cout << puzzels.size() << " sudoku puzzels" << endl;
	//cout << "Multi Thread " << endl;
	//sudoku.ReadFile("");
	/*ThreadStart(10);
	ThreadStart(9);
	ThreadStart(8);
	ThreadStart(7);
	ThreadStart(6);
	ThreadStart(5);
	ThreadStart(4);
	ThreadStart(3);
	ThreadStart(2);*/
	//ThreadStart(1);

	SuDoKu sudoku;// = new Sudoku();
	sudoku.ReadFile(puzzels[0]);
	cout << "Current Board " << endl;
	sudoku.DrawBoard();

	time_t start = time(0);
	sudoku.SolveBruteForce();
	double seconds_since_start = difftime(time(0), start);

	cout << "SolveBruteForce Done in " << seconds_since_start << " Seconds." << endl;
	sudoku.DrawBoard();
	cout << "*************************" << endl;

	SuDoKu sudoku2;// = new Sudoku();
	sudoku2.ReadFile(puzzels[0]);

	start = time(0);
	sudoku2.SolveBruteForceV2();
	seconds_since_start = difftime(time(0), start);

	cout << "SolveBruteForceV2 Done in " << seconds_since_start << " Seconds." << endl;
	sudoku2.DrawBoard();
	cout << "*************************" << endl;

	SuDoKu sudoku3;// = new Sudoku();

	sudoku3.ReadFile(puzzels[0]);

	start = time(0);
	sudoku3.SolveBruteForceV2();
	seconds_since_start = difftime(time(0), start);

	cout << "SolveBruteForceV3 Done in " << seconds_since_start << " Seconds." << endl;// Solved " << counta << endl;
	sudoku3.DrawBoard();
	cout << "Done" << endl;


	//pause for any input
	int a;
	cin >> a;
	return 0;
}


int on;
int counta;

void ThreadStart(int number)
{
	on = puzzels.size();// -1;
	counta = 0;
	//const int num_threads = 6;
	vector<thread> t;
	//Launch a group of threads
	cout << "Threads=" << number << ". ";
	time_t start = time(0);
	for (int i = 0; i < number; ++i) {
		t.push_back(std::thread(ThreadPro));
	}

	//Join the threads with the main thread
	for (int i = 0; i < number; ++i) {
		t[i].join();
	}

	double seconds_since_start = difftime(time(0), start);
	cout << "Done in " << seconds_since_start << " Seconds. Solved " << counta << endl;
	//cout << "Solved " << counta << endl;
}

//Processing Thread method
void ThreadPro()
{
	while (on >= 0)
	{
		on--;
		if (on < 0)
			return;

		SuDoKu sudoku;// = new Sudoku();

		sudoku.ReadFile(puzzels[on]);
		//cout << "new " << endl;
		//sudoku.DrawBoard();
		if (sudoku.SolveBruteForce())
		{
			counta++;
		}
		//cout << "solved" << endl;
		//sudoku.DrawBoard();
	}
}