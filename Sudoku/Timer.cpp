#include "stdafx.h"
#include "Timer.h"

Timer::Timer()
{
	// get ticks per second
	QueryPerformanceFrequency(&frequency);
}
void Timer::start()
{
	// start timer
	QueryPerformanceCounter(&t1);
}
void Timer::end()
{
	QueryPerformanceCounter(&t2);
}
double Timer::elaspedTime()
{
	return elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
}